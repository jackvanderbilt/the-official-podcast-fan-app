import VueRouter from 'vue-router'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            name: 'episodes',
            alias: '/episodes',
            path: '/',
            meta: {
                primaryNavigatorHighlight: 'episodes'
            },
            component: () => import( '../vue/pages/episodes/page.vue' )
        },
        {
            name: 'episode',
            alias: '/episode',
            path: '/episode/:id?',
            meta: {
                primaryNavigatorHighlight: 'episodes'
            },
            component: () => import( '../vue/pages/episode/page.vue' )
        },
        {
            name: 'trivia',
            alias: [ 'wiki', 'wikipedia' ],
            path: '/trivia',
            meta: {
                primaryNavigatorHighlight: 'trivia'
            },
            component: () => import( '../vue/pages/trivia.vue' )
        },
        {
            name: 'fanfiction',
            alias: 'fanfiction',
            path: '/fanfics',
            meta: {
                primaryNavigatorHighlight: 'fanfics'
            },
            component: () => import( '../vue/pages/fanfics.vue' )
        },
        {
            name: 'soundboard',
            path: '/soundboard',
            meta: {
                primaryNavigatorHighlight: 'soundboard'
            },
            component: () => import( '../vue/pages/soundboard.vue' )
        }
    ]
})

export default router
