export default {
    namespaced: true,
    state: {
        episodes: [
            {
                title: 'Australian Dangers With FriendlyJordies',
                description: 'Five close man friends gather around to talk about Australian Dinosaurs.',
                thumbnail: '',
                platforms: {
                    spotify: 'https://open.spotify.com/episode/1b7CiSQ3MVd4L6mmLZHHq4?si=JCyvkUf0RuCHzGdXU8t-zw',
                    soundcloud: 'https://soundcloud.com/theofficialpodcast/episode-187-australian-dangers-with-friendlyjordies',
                    youtube: 'https://www.youtube.com/watch?v=vV-NR4GXn-I',
                    googlePlay: 'https://play.google.com/music/m/Iv4af6j46ldkjja7vwnvljbyiw4?t=The_Official_Podcast',
                    googlePodcasts: ' https://www.google.com/podcasts?feed=aHR0cHM6Ly9mZWVkcy5tZWdhcGhvbmUuZm0vVE9QNzc4NDYyNTk4MA%3D%3D',
                    itunes: 'https://podcasts.apple.com/nl/podcast/187-australian-dangers-with-friendlyjordies/id1186089636?i=1000482244982'
                },
                guests: [
                    {
                        name: 'FriendlyJordies',
                        socials: [
                            {
                                platform: 'twitter',
                                link: 'https://twitter.com/friendlyjordies'
                            }
                        ]
                    }
                ],
                sponsors: [
                    {
                        name: 'Anchor',
                        description: 'Anchor: The easiest way to make a podcast.',
                        link: 'https://anchor.fm/app'
                    }
                ]
            }
        ]
    },

    actions: {

    },

    mutations: {
        setEpisodes( state, payload )
        {
            state.episodes = payload
        }
    },

    getters: {
        episodes( state )
        {
            return state.episodes
        }
    }
}
