import Vue from 'vue'
import Vuex from 'vuex'

import TopNavigator from './topNavigator.js'
import Episodes from './episodes.js'

Vue.use( Vuex )

export default new Vuex.Store({
    modules: {
        topNavigator: TopNavigator,
        episodes: Episodes
    }
})
