export default {
    namespaced: true,
    state: {
        label: 'Episodes',
        icons: [{
            icon: 'search',
            action: () => 'toggle search <o/'
        }]
    },

    actions: {

    },

    mutations: {
        setIcons( state, payload )
        {
            state.icons = payload
        },
        setLabel( state, payload )
        {
            // eslint-disable-next-line no-param-reassign
            state.label = payload
        }
    },

    getters: {
        label( state )
        {
            return state.label
        },
        icons( state )
        {
            return state.icons
        }
    }
}
