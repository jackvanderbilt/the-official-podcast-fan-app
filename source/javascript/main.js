// Babel polyfill
import 'regenerator-runtime/runtime'

// Plugins
import './fontawesome.js'
import './vue-touch-events.js'

// Vue related imports
import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuelidate from 'vuelidate'

// APP
import App from '../vue/App.vue'
import AppRouter from '../vue/router'

import AppStore from '../vue/vuex/store'

Vue.use( VueRouter )
Vue.use( Vuelidate )

document.addEventListener( 'DOMContentLoaded', () =>
{
    new Vue({
        render: ( h ) => h( App ),
        router: AppRouter,
        store: AppStore
    }).$mount( document.querySelector( 'html > body > main' ) )
})
