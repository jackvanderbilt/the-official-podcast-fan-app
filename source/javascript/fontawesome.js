import Vue from 'vue'

import { library as FALibrary } from '@fortawesome/fontawesome-svg-core'
import { faPodcast } from '@fortawesome/pro-regular-svg-icons/faPodcast'
import { faQuestion } from '@fortawesome/pro-regular-svg-icons/faQuestion'
import { faBook } from '@fortawesome/pro-regular-svg-icons/faBook'
import { faHashtag } from '@fortawesome/pro-regular-svg-icons/faHashtag'
import { faPlay } from '@fortawesome/pro-regular-svg-icons/faPlay'
import { faSearch } from '@fortawesome/pro-regular-svg-icons/faSearch'
import { faBars } from '@fortawesome/pro-regular-svg-icons/faBars'
import { faFilter } from '@fortawesome/pro-regular-svg-icons/faFilter'
import { faSortAlt } from '@fortawesome/pro-regular-svg-icons/faSortAlt'
import { faExclamationTriangle } from '@fortawesome/pro-regular-svg-icons/faExclamationTriangle'
import { faTimes } from '@fortawesome/pro-regular-svg-icons/faTimes'
import { faVolume } from '@fortawesome/pro-regular-svg-icons/faVolume'
import { faComments } from '@fortawesome/pro-regular-svg-icons/faComments'
import { faArrowUp } from '@fortawesome/pro-regular-svg-icons/faArrowUp'
import { faChevronDown } from '@fortawesome/pro-regular-svg-icons/faChevronDown'
import { faChevronUp } from '@fortawesome/pro-regular-svg-icons/faChevronUp'
import { faExternalLinkAlt } from '@fortawesome/pro-regular-svg-icons/faExternalLinkAlt'
import { faCog } from '@fortawesome/pro-regular-svg-icons/faCog'
import { faCommentAltLines } from '@fortawesome/pro-regular-svg-icons/faCommentAltLines'
import { faReddit } from '@fortawesome/free-brands-svg-icons/faReddit'
import { faDiscord } from '@fortawesome/free-brands-svg-icons/faDiscord'
import { faPatreon } from '@fortawesome/free-brands-svg-icons/faPatreon'
import { faSpotify } from '@fortawesome/free-brands-svg-icons/faSpotify'
import { faSoundcloud } from '@fortawesome/free-brands-svg-icons/faSoundcloud'
import { faItunes } from '@fortawesome/free-brands-svg-icons/faItunes'
import { faTwitter } from '@fortawesome/free-brands-svg-icons/faTwitter'
import { faInfo } from '@fortawesome/pro-regular-svg-icons/faInfo'
import { FontAwesomeIcon, FontAwesomeLayers } from '@fortawesome/vue-fontawesome'

FALibrary.add( faPodcast )
FALibrary.add( faQuestion )
FALibrary.add( faBook )
FALibrary.add( faHashtag )
FALibrary.add( faPlay )
FALibrary.add( faSearch )
FALibrary.add( faBars )
FALibrary.add( faFilter )
FALibrary.add( faSortAlt )
FALibrary.add( faExclamationTriangle )
FALibrary.add( faTimes )
FALibrary.add( faVolume )
FALibrary.add( faComments )
FALibrary.add( faArrowUp )
FALibrary.add( faChevronDown )
FALibrary.add( faChevronUp )
FALibrary.add( faExternalLinkAlt )
FALibrary.add( faCog )
FALibrary.add( faReddit )
FALibrary.add( faDiscord )
FALibrary.add( faPatreon )
FALibrary.add( faSpotify )
FALibrary.add( faSoundcloud )
FALibrary.add( faItunes )
FALibrary.add( faTwitter )
FALibrary.add( faInfo )
FALibrary.add( faCommentAltLines )

Vue.component( 'font-awesome-icon', FontAwesomeIcon )
Vue.component( 'font-awesome-layers', FontAwesomeLayers )
